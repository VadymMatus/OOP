#include "pch.h"
#include "D.h"
#include <iostream>

D::D(int n) : m_n(n)
{
	m_p = new int[m_n];
	for (int i = 0; i < m_n; i++) m_p[i] = 0;
}

D::D(D& o) : m_n(o.m_n)
{
	m_p = new int[m_n];
	for (int i = 0; i < m_n; i++) m_p[i] = o.m_p[i];
}

D :: ~D() { delete[] m_p; }

int D::getN(void) const { return m_n; }

int& D::operator[](const unsigned int i) const
{
	return m_p[i];
}

D& D::operator=(D& right)
{
	if (m_n != right.m_n)
	{
		delete[] m_p;
		m_n = right.m_n;
		m_p = new int[m_n];
	}

	for (int i = 0; i < m_n; i++)
		m_p[i] = right.m_p[i];

	return *this;
}

D D::operator+(const D& right) const
{
	D tmp(m_n);
	for (int i = 0; i < tmp.m_n; i++)
		if (right.getN() > i)
			tmp.m_p[i] = m_p[i] + right.m_p[i];
	return tmp;
}

D D::operator+(const int& right) const
{
	D tmp(m_n);
	for (int i = 0; i < tmp.m_n; i++)
		tmp.m_p[i] = m_p[i] + right;
	return tmp;
}

D D::operator-(const D& right) const
{
	D tmp(m_n);
	for (int i = 0; i < tmp.m_n; i++)
		if (right.getN() > i)
			tmp.m_p[i] = m_p[i] - right.m_p[i];

	return tmp;
}
