// Laboratory_2_2.cpp : Этот файл содержит функцию "main". Здесь начинается и заканчивается выполнение программы.
//

#include "pch.h"
#include <iostream>

/**
 * @fn	void metod1(CR1 &cr);
 *
 * @brief	Metod 1
 *
 * @param [in,out]	cr	The carriage return.
 */
void metod1(CR1 &cr);

/**
 * @fn	void metod2(CR1 &cr);
 *
 * @brief	Metod 2
 *
 * @param [in,out]	cr	The carriage return.
 */
void metod2(CR1 &cr);

int main()
{
	CR1 cr1;

	do
	{
		system("cls");
		std::cout << "\t<< Menu >>" << std::endl
			<< "1. call method1" << std::endl
			<< "2. call method2" << std::endl
			<< "3. exit" << std::endl
			<< "->";
		
		switch (getchar())
		{
		case '1':
			metod1(cr1);
			break;
		case '2':
			metod2(cr1);
			break;
		case '3':
			return EXIT_SUCCESS;
		default:
			break;
		}

		while (getchar() != '\n') continue;

	} while (true);

	return EXIT_SUCCESS;
}

void metod1(CR1 &cr)
{
	float answer(0);

	std::cout << "Enter value -> ";
	while (!(std::cin >> answer))
	{
		std::cin.clear();
		while (getchar() != '\n') continue;
		std::cout << "Error: enter integer value!" << std::endl
			<< "Enter value -> ";
	}

	std::cout << "cr.metod1(value) -> " << cr.metod1(answer) << std::endl;
	getchar();
}

void metod2(CR1 &cr)
{
	float answer(0);

	std::cout << "Enter value -> ";
	while (!(std::cin >> answer))
	{
		std::cin.clear();
		while (getchar() != '\n') continue;
		std::cout << "Error: enter integer value!" << std::endl
			<< "Enter value -> ";
	}

	std::cout << "cr.metod2(value) -> " << cr.metod2(answer) << std::endl;
	getchar();
}