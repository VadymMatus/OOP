#include "pch.h"
#include <iostream>

int main()
{
	int d;

	MyClass *m = new MyClass();
	MyClass N;

    std::cout << "\nRashet factorial dlja klassa m \n" << std::endl;
	std::cout << "\nVvedite d\n" << std::endl;
	std::cin >> d;
	std::cout << "\nFactorial m->factorial(d) = " << m->factorial(d) << std::endl;

	std::cout << "\nRashet factorial dlja klassa N\n" << std::endl;
	std::cout << "\nVvedite d\n" << std::endl;
	std::cin >> d;
	std::cout << "\nFactorial N.factorial(d) = " << N.factorial(d) << std::endl;

	delete m;
	return 0;
}