#include "pch.h"

using namespace std;
typedef float S;

void swapR(int&, int&);
void swapR(char&, char&);
void reverseArray(int*, int = 10);
int* reverseArray(const int*, int, int);
char* reverseArray(const char*);
void view(const int*, int = 5);
void task1();
void task2();

/**
 * @fn	S square(size_t side1 = 0, size_t side2 = 0, size_t side3 = 0);
 *
 * @brief	Вычисление площади треугольника
 *
 * @param	side1	(Optional) The first side.
 * @param	side2	(Optional) The second side.
 * @param	side3	(Optional) The third side.
 *
 * @return	A Площать.
 */
S square(size_t side1 = 0, size_t side2 = 0, size_t side3 = 0);

/**
 * @fn	S square(float base1 = 0.0f, float base2 = 0.0f, size_t height = 0);
 *
 * @brief	Вычисление площади трапеции
 *
 * @param	base1 	(Optional) The first base.
 * @param	base2 	(Optional) The second base.
 * @param	height	(Optional) The height.
 *
 * @return	A Площадь.
 */
S square(float base1, float base2, float height);

/**
 * @fn	S square(float base = 0.0f, size_t height = 0);
 *
 * @brief	Вычисление площади ромба
 *
 * @param	base  	(Optional) The base.
 * @param	height	(Optional) The height.
 *
 * @return	A Площадь.
 */
S square(float base = 0.0f, float height = 0.0f);

int main()
{
	setlocale(LC_ALL, "RU");

	do
	{
		system("cls");
		std::cout << "\t<< Меню >>" << std::endl
			<< "1. Задача 1" << std::endl
			<< "2. Задача 2" << std::endl
			<< "3. Выход" << std::endl
			<< "->";

		switch (getchar())
		{
		case '1':
			while (getchar() != '\n') continue;
			task1();
			break;
		case '2':
			while (getchar() != '\n') continue;
			task2();
			break;
		case '3':
			return EXIT_SUCCESS;
		default:
			break;
		}

		while (getchar() != '\n') continue;

	} while (true);

	return EXIT_SUCCESS;
}

void task2()
{
	size_t triagle_side1, triagle_side2, triagle_side3;
	float trapezoid_base1, trapezoid_base2, diamond_base,
		diamond_height, trapezoid_height;

	do
	{
		system("cls");
		std::cout << "\t<< Меню >>" << std::endl
			<< "1. Вычисление площади треугольника" << std::endl
			<< "2. Вычисление площади трапеции" << std::endl
			<< "3. Вычисление площади ромба" << std::endl
			<< "4. Выход" << std::endl
			<< "->";

		switch (getchar())
		{
		case '1':
			while (getchar() != '\n') continue;
			cout << "Вычисление площади треугольника:" << endl
				<< '\t' << "Длина 3-х сторон треугольника (через пробел)/> ";
			while (!(cin >> triagle_side1 >> triagle_side2 >> triagle_side3))
			{
				cin.clear();
				while (getchar() != '\n') continue;
				cout << '\t' << "Длина 3-х сторон треугольника (через пробел)/> ";
			}
			cout << "Площадь треугольника => " << square(triagle_side1, triagle_side2, triagle_side3) << endl;
			getchar();
			break;
		case '2':
			while (getchar() != '\n') continue;
			cout << "Вычисление площади трапеции:" << endl
				<< '\t' << "Длина двох оснований трапеции (через пробел)/> ";
			while (!(cin >> trapezoid_base1 >> trapezoid_base2))
			{
				cin.clear();
				while (getchar() != '\n') continue;

				cout << '\t' << "Длина двох оснований трапеции (через пробел)/> ";
			}
			cout << '\t' << "Высота трапеции /> ";
			while (!(cin >> trapezoid_height))
			{
				cin.clear();
				while (getchar() != '\n') continue;

				cout << '\t' << "Высота трапеции /> ";
			}
			cout << "Площадь трапеции => " << square(trapezoid_base1, trapezoid_base2, trapezoid_height) << endl;
			getchar();
			break;
		case '3':
			cout << "Вычисление площади ромба:" << endl
				<< '\t' << "Длина основаны ромба /> ";
			while (!(cin >> diamond_base))
			{
				cin.clear();
				while (getchar() != '\n') continue;

				cout << '\t' << "Длина основаны ромба /> ";
			}
			cout << '\t' << "Высота ромба /> ";
			while (!(cin >> diamond_height))
			{
				cin.clear();
				while (getchar() != '\n') continue;

				cout << '\t' << "Высота ромба /> ";
			}
			cout << "Площадь ромба  => " << square(diamond_base, diamond_height) << endl;
			getchar();
			break;
		case '4':
			return;
		default:
			break;
		}

		while (getchar() != '\n') continue;

	} while (true);
}

S square(size_t side1, size_t side2, size_t side3)
{
	float p((float)(side1 + side2 + side3) / 2),
		rez = p * (p - side1)*(p - side2)*(p - side3);

	if (rez < 0)
	{
		cout << "Число не может быть отрицательным!" << endl;
		return 0.0f;
	}

	return sqrtf(rez);
}

S square(float base1, float base2, float height)
{
	if ((base1 + base2) <0)
	{
		cout << "Проверте даные!" << endl;
		return 0.0f;
	}

	return ((base1 + base2) / 2) + height;
}

S square(float base, float height)
{
	if (height < 0)
	{
		cout << "Высота не может быть отрицательной!" << endl;
		return 0.0f;
	}

	return base * height;
}

void task1()
{
	int ar[10] = { 1, 2, 3, 4, 5, 6 };
	cout << "Initial array\n";
	view(ar, 10);

	// реверс усього масиву
	reverseArray(ar);
	cout << "\nReverse array\n";
	view(ar, 10);

	// реверс частини масиву
	int* p = reverseArray(ar, 5, 9);
	cout << "\nReverse part array\n";
	view(p);

	//реверс стрічки
	const char *s = "NOMEL ON NOLEM ON";
	char *pr = reverseArray(s);
	cout << "\nInitial string\n";	cout << s << endl;
	cout << "\nReverse string\n";	cout << pr << endl;

	delete[] p;
	//delete s;
	//delete pr;
}

// перестановка за допомогою посилань (ссылок)
void swapR(int& rx, int& ry)
{
	int t = rx; rx = ry; ry = t;
}

void swapR(char& rx, char& ry)
{
	char t = rx; rx = ry; ry = t;
}

// реверс массиву в самому собі
void reverseArray(int* p, int size)
{
	for (int i = 0, j = size - 1; i < j; i++, j--) swapR(p[i], p[j]);
}

// формування зворотнього масиву із частини масиву
int* reverseArray(const int* pIn, int top, int end) {
	if (end <= top) return 0;

	int size = end - top + 1;
	int* pRes = new int[size];
	int i, j, ir, jr;

	for (i = top, j = end, ir = 0, jr = size - 1; i < j; ) {
		pRes[ir++] = pIn[j--];
		pRes[jr--] = pIn[i++];
	}

	if (i == j) pRes[ir] = pIn[i];

	return pRes;
}

// реверс стрічки
char* reverseArray(const char* p) {

	char* pOut = new char[strlen(p) + 1];
	strncpy_s(pOut, strlen(pOut), p, strlen(p) + 1);
	char *pTop = pOut,
		*pEnd = pOut + strlen(pOut) - 1;

	while (pTop < pEnd) swapR(*pTop++, *pEnd--);

	return pOut;
}

void view(const int* arr, int count)
{
	for (int i = 0; i < count; i++)
		cout << endl << setw(6) << i << "    " << arr[i];
}