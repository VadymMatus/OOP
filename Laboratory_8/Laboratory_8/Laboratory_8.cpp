﻿// Laboratory_8.cpp : вариант №5

#include "pch.h"

using namespace std;

void task1();
void task2();
void task3();

/**
 * @brief Функция ввода и проверки коректности ввода.
 *  Являеться обёрткой для упрощения ввода чесел,
 *  в случае если ввода строки заместь числа, выведет
 *  сообщение и попросит повторить ввод.
 *
 * @tparam Args Стандартный тип даных, не рекомендуеться
 *  использовать строки.
 * @tparam T Строковый тип данных.
 * @param args Переменые для ввода
 * @param message Сообщение в случае ошибки
 */
template <typename T, typename... Args>
void input_check(const T &message, Args &... args)
{
	static_assert(std::is_same<T, char>::value ||
		std::is_same<T, std::string>::value ||
		std::is_array<T>::value,
		"For message, use string types!");

	std::cout << message;

	while (!(std::cin >> ... >> args))
	{
		std::cin.clear();
		cin.ignore(numeric_limits<streamsize>::max(), '\n'); // очистка буфера ввода

		std::cout << "Error: " << /* (std::is_same<T, std::string>::value ? message.c_str() : */ message /* ) */;
	}

	std::cin.ignore(numeric_limits<streamsize>::max(), '\n'); // очистка буфера ввода
}

/**
 * Функция ввода и проверки коректности ввода.
 *  Являеться обёрткой для упрощения ввода чесел,
 *  в случае если ввода строки заместь числа, выведет
 *  сообщение и попросит повторить ввод.
 *
 * @tparam T Стандартный тип
 * @param value Переменая для ввода
 * @param message Сообщение в случае ошибки
 */
template <typename T>
void input_check(T &value, const std::string message)
{
	while (!(std::cin >> value))
	{
		std::cin.clear();
		while (getchar() != '\n') continue; // очистка буфера ввода

		std::cout << message.c_str();
	}

	while (getchar() != '\n') continue;  // очистка буфера ввода
}

/**
 * Функция вывода елементов масива в рад.
 *
 * @tparam T Стандартный тип
 * @param p Массив для вывода
 * @param size Размер выводимого массива
 */
template <class T>
void view(const T *p, const size_t size)
{
	for (register size_t i(0); i < size; i++)
		cout << p[i] << '\t';

	cout << endl;
}

/**
 * В начало массива записываются отрицательные элементы
 * после отрицательных элементов записываются положительные элементы
 *
 * @tparam T Стандартный тип
 * @param p Массив для вывода
 * @param size Размер выводимого массива
 */
template <class T>
void reorg(T *p, const int size)
{
	T tmp;
	for (register size_t i(0), j(0); i < size; i++)
	{
		if (p[i] > 0) continue;

		tmp = p[i]; // первый отрицательный елемент

		// смещение масива влево
		for (register size_t k(i); k > j; k--) p[k] = p[k - 1];

		p[j] = tmp; // вставка отрицательного елемента в начало масива
		j++;
	}
}

/**
 * Якщо в масиві зустрічаються негативні елементи,
 * то їх значення змінюється на протилежне.
 *
 * @tparam T Стандартный тип
 * @param array Массив для переобразования
 * @param size Размер массива
 */
template <typename T>
void replace_neg_element(T *array, const size_t size)
{
	for (register size_t i(0); i < size; i++)
		if (array[i] < 0)
			array[i] = abs(array[i]);
}

int main()
{
	do
	{
		cout << endl << '\t' << "MENU" << endl
			<< "1. Task one" << endl
			<< "2. Task two" << endl
			<< "3. Task three" << endl
			<< "4. Exit" << endl
			<< "Answer /> ";

		switch (input_check<int>("Error: Answer /> "))
		{
		case 1:
			task1();
			break;
		case 2:
			task2();
			break;
		case 3:
			task3();
			break;
		case 4:
			return EXIT_SUCCESS;
		default:
			break;
		}
	} while (true);
}

void task1()
{
	int ia[] = { 1, -3, 0, -9, 2, 7, 0, -19 };
	double da[] = { 0.0875, 1.25, -3, 0.0, -7.986 };
	long la[] = { 1L, -2L, 3L, -100L };
	float fa[] = { -2.12, 1.5, -3.25, -17.2 };
	char s[] = "Vivat!";

	cout << "\nint Array\n";
	view(ia, sizeof(ia) / sizeof(int));

	cout << "\ndouble Array\n";
	view(da, sizeof(da) / sizeof(double));

	cout << "\nlong Array\n";
	view(la, sizeof(la) / sizeof(long));

	cout << "\nchar Array\n";
	view(s, strlen(s));
}

void task2()
{
	long la[] = { 1L, -2L, 3L, -100L, -200L };
	float fa[] = { -2.12, 1.5, -3.25, -17.2 };
	int n = sizeof(la) / sizeof(long);

	cout << "\nInitial long array\n";	view(la, n);
	cout << "Reorganization\n"; reorg(la, n); view(la, n);

	n = sizeof(fa) / sizeof(float);
	cout << "\nInitial float array\n";	view(fa, n);
	cout << "Reorganization\n"; reorg(fa, n); view(fa, n);
}

void task3()
{
	int *iarray;
	float *farray;
	size_t isize(0), fsize(0);

	cout << endl << "Enter size array (int) /> ";
	input_check(isize, "Error: Enter size array (int) /> ");
	iarray = new int[isize];

	cout << endl << "Enter size array (float) /> ";
	input_check(fsize, "Error: Enter size array (float) /> ");
	farray = new float[fsize];

	cout << "Fill (int) array[" << isize << "]: " << endl;
	for (register size_t i(0); i < isize; i++)
	{
		cout << "array[" << i << "] /> ";
		input_check(iarray[i], "Error:  repeat attempt />");
	}

	cout << endl << "Fill (float) array[" << fsize << "]: " << endl;
	for (register size_t i(0); i < fsize; i++)
	{
		cout << "array[" << i << "] /> ";
		input_check(farray[i], "Error:  repeat attempt />");
	}

	cout << endl << "Your array (int) : " << endl;
	view(iarray, isize);

	replace_neg_element(iarray, isize);
	cout << "Replace negative element, to positive: " << endl;
	view(iarray, isize);


	cout << endl << "Your array (float) : " << endl;
	view(farray, fsize);

	replace_neg_element(farray, fsize);
	cout << "Replace negative element, to positive: " << endl;
	view(farray, fsize);

	cout << endl << "Repeat [y/N]? /> ";
	if (getchar() == 'y') task3();
}
