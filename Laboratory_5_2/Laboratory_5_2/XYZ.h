#pragma once

typedef float T;

class XYZ
{
	T x, y, z, result;
public:
	XYZ();
	XYZ(T x, T y, T z);

	void set_x(T x);
	void set_y(T y);
	void set_z(T z);

	friend void run(XYZ &xyz);
	friend void print(XYZ &xyz);
};