#include "pch.h"
#include "DF.h"
#include <iostream>

using namespace std;

DF::DF() {}

DF::DF(int razmer) : raz_mas(razmer)
{
	cout << "raz_mas=" << raz_mas << endl;
	mas = new int[raz_mas];
	cout << "Constructor s param" << endl;
}

DF::~DF()
{
	delete[] mas;
	cout << "Destructor" << endl;
}

void DF::SetMas()
{
	cout << "raz_mas=" << raz_mas << endl;
	
	for (int i(0); i < raz_mas; i++)
	{
		cout << "Vvod" << i << endl;
		cin >> mas[i];
	}

	cout << "Massiv vveden" << endl;
}

void DF::PrintMas()
{
	cout << "\tElementy massiva" << endl;
	
	for (int i(0); i < raz_mas; i++)
	{
		if (i % 5 == 0) cout << " " << endl;
		cout << "\t" << mas[i];
	}

	cout << " " << endl;
}