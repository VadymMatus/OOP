#include "pch.h"
#include "Stat_m.h"


Stat_m::Stat_m(const char* s)
{
	strncpy_s(m_s, 40, s, 40);
	incObj();
}

Stat_m::Stat_m()
{
	m_s[0] = '\0';
	incObj();
}
Stat_m::~Stat_m()
{
	--objAmount;
}

void Stat_m::initAmount(const int v)
{
	objAmount = v;
}

int Stat_m::getAmount()
{
	return objAmount;
}

int Stat_m::incObj()
{
	return ++objAmount;
}

int Stat_m::lenS()
{
	return strlen(m_s);
}
