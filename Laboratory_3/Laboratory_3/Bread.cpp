#include "pch.h"
#include "Bread.h"


Bread::Bread()
{
	price = 0.0f;
	weight = 0.0f;
	name = "";
}

void Bread::setName(std::string name)
{
	this->name = name;
}


std::string Bread::getName()
{
	return name;
}


void Bread::setPrice(float price)
{
	this->price = price;
}


float Bread::getPrice()
{
	return price;
}


void Bread::setWeight(float weight)
{
	this->weight = weight;
}


float Bread::getWeight()
{
	return weight;
}