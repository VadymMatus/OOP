#pragma once

class CBook
{
private:
	/** @brief	����� */
	char m_author[50];
	/** @brief	�������� �� ����� */
	char *m_pTitle;
	/** @brief	�� ������� */
	int m_year;
public:

	CBook();
	~CBook();

	void setAuthor(const char*);

	void setTitle(const char*);

	void setYear(const int);

	char* getAuthor(void);

	char* getTitle(void);

	int getYear(void);

};

