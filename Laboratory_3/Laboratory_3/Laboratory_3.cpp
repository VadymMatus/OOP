// Лабораторная работа №3, Вариант №5, см. "\docs\ATask.doc"

#include "pch.h"
#include "Bread.h"
#include <string>

/**
 * @fn	void view1(const char *s, CBook &o)
 *
 * @brief	Вывод информании о книге
 *
 * @param 		  	s	Название переменной для образного представления.
 * @param [in,out]	o	Объект заполненого класса CBook.
 */
void view1(const char*, CBook&);

/**
 * @fn	void view2(Bread &bread)
 *
 * @brief	Вывод информации о хлебоизделии
 *
 * @param [in,out]	bread	Заполненый объект класса Bread.
 */
void view2(Bread &bread);

/**
 * @fn	void task1(void)
 *
 * @brief	Первое задание лабораторной работы см. "/docs/ATask.doc"
 */
void task1(void);

/**
 * @fn	void task2(void)
 *
 * @brief	Второе задание лабораторной работы см. "/docs/ATask.doc"
 */
void task2(void);

/**
 * @brief	Main entry-point for this application
 * @return	Exit-code for the process - 0 for success, else an error code.
 */
int main()
{
	do
	{
		system("cls");
		std::cout << "\t<< Menu >>" << std::endl
			<< "1. task 1" << std::endl
			<< "2. task 2" << std::endl
			<< "3. exit" << std::endl
			<< "->";

		switch (getchar())
		{
		case '1':
			while (getchar() != '\n') continue;
			task1();
			break;
		case '2':
			while (getchar() != '\n') continue;
			task2();
			break;
		case '3':
			return EXIT_SUCCESS;
		default:
			break;
		}

		system("pause");

		while (getchar() != '\n') continue;

	} while (true);


	return 0;
}

void view1(const char *s, CBook &o)
{
	std::cout << std::endl << "State of object \' " << s << " \'" << std::endl
		<< "Author:\t" << o.getAuthor() << std::endl
		<< "Title:\t" << o.getTitle() << std::endl
		<< "Year:\t" << o.getYear() << std::endl;
}

void view2(Bread &bread)
{
	std::cout << "Breat \"" << bread.getName().c_str() << "\":" << std::endl
		<< "\tPrice: " << bread.getPrice() << std::endl
		<< "\tWeight: " << bread.getWeight() << std::endl;
}

void task1(void)
{
	std::cout << std::endl << "Rabotaet Lab3.cpp " << std::endl;
	CBook book;

	char stroka_vvoda[10];
	std::cout << std::endl << "Vvedi avtora" << std::endl;
	std::cin >> stroka_vvoda;
	book.setAuthor(stroka_vvoda);
	std::cout << "Otrabotal setAuthor pole = " << book.getAuthor() << std::endl;

	book.setTitle("Object-Oriented Programming in C++");
	std::cout << std::endl << "Otrabotal setTitle pole = " << book.getTitle() << std::endl;

	book.setYear(2004);

	view1("book", book);
	std::cout << std::endl << "Otrabotal view " << std::endl;

	system("pause");

	std::cout << std::endl << "===== Dinamic CBook =====" << std::endl;
	CBook *cbook = new CBook();
	int year(0);

	std::cout << std::endl << "Enter autor: ";
	std::cin >> stroka_vvoda;
	cbook->setAuthor(stroka_vvoda);

	std::cout << std::endl << "Enter title: ";
	std::cin >> stroka_vvoda;
	cbook->setTitle(stroka_vvoda);

	std::cout << std::endl << "Enter year: ";
	std::cin >> year;
	cbook->setYear(year);

	view1("cbook", *cbook);
	delete cbook;
}

void task2(void)
{
	Bread sbread,
		*pbread = new Bread();
	
	std::string svalue;
	float fvalue = 0.0f;

	//Заполнение статического объекта
	std::cout << "Fill static \"sbread\":" << std::endl
		<< "\tName -> ";
	std::getline(std::cin, svalue);
	sbread.setName(svalue);

	std::cout << "\tPrice -> ";
	std::cin >> fvalue;
	sbread.setPrice(fvalue);

	std::cout << "\tWeight -> ";
	std::cin >> fvalue;
	sbread.setWeight(fvalue);
	view2(sbread); // вывод
	
	//Заполнение динамического объекта
	while (getchar() != '\n') continue;
	std::cout << "Fill dinamic \"sbread\":" << std::endl
		<< "\tName -> ";
	std::getline(std::cin, svalue);
	pbread->setName(svalue);

	std::cout << "\tPrice -> ";
	std::cin >> fvalue;
	pbread->setPrice(fvalue);

	std::cout << "\tWeight -> ";
	std::cin >> fvalue;
	pbread->setWeight(fvalue);
	view2(*pbread); // вывод

	delete pbread;
}
